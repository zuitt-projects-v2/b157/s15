console.log("Hello World!");

/*prompt for 2 numbers and perform different arithmetic. Use console.warn for a total of 9 or less and alert for a total of 10 or greater*/

function getNumber() {
	let num1 = parseInt(prompt("What is the first number?"));
	let num2 = parseInt(prompt("What is the second number?"));
	let sum = num1 + num2;

	if (sum < 10) {
		let sum = num1 + num2;
		console.warn('The sum of two numbers is: ' + sum);	
	}

	else if (sum <= 20) {
		let difference = num1 - num2;
		alert('The difference of two numbers is: ' + difference);
	}

	else if (sum <= 29) {
		let product = num1 * num2;
		alert('The product of two numbers is: ' + product);
	}

	else if (sum >= 30) {
		let quotient = num1 / num2;
		alert('The quotient of two numbers is: ' + quotient);
	}

}

/*Prompt the user their name and age. If name and age is blank/null, alert*/

function userInfo() {
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");

	if (name == "" || age == "") {
		console.log("Are you a time traveler?");
	}
	else {
		console.log(`${name} is ${age} years old`);
	}
}

/* create a function name isLegalAge then prompt*/

function isLegalAge() {
	let age = prompt("Are you sure of your answer? Enter your age again.");

	if (age => 18) {
		alert("You are of legal age.");
	}
	else {
		alert("You are not allowed here");
	}
}

/*create a switch statement that will check the user's age input*/

function ageChecker() {
	let age = prompt("Just want to make sure, how old are you again?");
	let response;

	switch (age) {
		case "18":
		response = "You are now allowed to party.";
		break;
		case "21":
		response = "You are now part of the adult society.";
		break;
		case "65":
		response = "We thank you for your contribution to society."
		break;
		default:
		response = "Are you sure you're not an alien?"
	}
	alert(response);
}

/*Try-catch-finally*/

function isLegalAge(){
	let userInput = prompt("How old are you?");

	try {
		if (userInput === '' ) throw 'the input is empty';
		if (isNaN(userInput)) throw 'the input is Not a Number';
		if (userInput <= 0) throw 'Not a valid Input' 
		if (userInput <= 7) throw 'the input is good for preschool'; 
		if (userInput > 7) throw 'too old for preschool';

	} catch(error) {
		console.warn(error.message);

	} finally {
		alert('This is from the finally section.');
	}
}

